package com.banque;

import java.util.List;

public class Compte {

    private  String rib;
    private Client client;
    private List<Debit> debitList ;
    private  List<Credit> creditList;


    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Debit> getDebitList() {
        return debitList;
    }

    public void setDebitList(List<Debit> debitList) {
        this.debitList = debitList;
    }

    public List<Credit> getCreditList() {
        return creditList;
    }

    public void setCreditList(List<Credit> creditList) {
        this.creditList = creditList;
    }

    public Compte(String rib, Client client, List<Debit> debitList, List<Credit> creditList) {
        this.rib = rib;
        this.client = client;
        this.debitList = debitList;
        this.creditList = creditList;

    }

    @Override
    public String toString() {
        return "Compte{" +
                "rib='" + rib + '\'' +
                ", client=" + client +
                ", debitList=" + debitList +
                ", creditList=" + creditList +
                '}';
    }


    public void consulterSolde()
    {
        Double solde = 0.0;
        for (Debit debit:debitList
             ) {

            solde+=debit.getValue();

        }

        for (Credit credit:creditList
                ) {

            solde-=credit.getValue();

        }

        System.out.print(solde);
    }
}
