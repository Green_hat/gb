package com.banque;

import java.time.LocalDateTime;

public class Debit {



    private Double value;
    private LocalDateTime dateTime;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Debit{" +
                "value=" + value +
                ", dateTime=" + dateTime +
                '}';
    }

    public Debit(Double value) {
        this.value = value;
        this.dateTime = LocalDateTime.now();
    }
}
