package com.cynapsys.banque;

import com.banque.Client;
import com.banque.Compte;
import com.banque.Credit;
import com.banque.Debit;

import java.util.ArrayList;
import java.util.List;

public class GestionCompte {

    public static  void main(String[] args)
    {
        String rib = "1354687987";
        Client client = new Client("12345678","Khaled", "Ben Hafsya");
        List<Debit> debitList = new ArrayList<Debit>();
        List<Credit> creditList = new ArrayList<Credit>();
        Compte compte = new Compte(rib, client, debitList, creditList) ;
        compte.getDebitList().add(new Debit(200.0));

        compte.consulterSolde();
    }
}
